﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OrdersService.Repositories
{
    public class OrdersRepository
    {

        public static bool AddOrders(Order order)
        {

            try
            {
                using (DBORDERSEntities entities = new DBORDERSEntities())
                {
                    entities.Orders.Add(order);
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}