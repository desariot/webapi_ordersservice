﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using OrdersService.Processors;

namespace OrdersService.Controllers
{
    public class OrdersRepositoryController : ApiController
    {
        /// <summary>
        /// Here u can create an order
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        /// 

        [HttpPost]
        [Route("SaveOrders")]
        public bool SaveOrders(Order order)
        {
           if(order == null)
           {
               return false;
           }
           return OrdersProcessor.ProcessOrders(order);
        }
    }
}