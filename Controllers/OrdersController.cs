﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OrdersService.Controllers
{
    public class OrdersController : ApiController
    {
        /// <summary>
        /// Get all orders from table Order
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Order> Get()
        {
            using (DBORDERSEntities entities = new DBORDERSEntities())
            {
                return entities.Orders.ToList();
            }
        }

        /// <summary>
        /// Get orders by id parameter
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public HttpResponseMessage Get(int id)
        {
            using (DBORDERSEntities entitites = new DBORDERSEntities())
            {
                var entity = entitites.Orders.FirstOrDefault(e => e.OrderID == id);

                if (entity != null)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, entity);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound,
                        "Order with ID = " + id.ToString() + " not found");
                }
            }
        }

        /// <summary>
        /// Get orders by datetime
        /// </summary>
        /// <param name="datetime"></param>
        /// <returns></returns>
        [Route("api/Orders/GetOrdersByDate")]
        [HttpGet]
        public HttpResponseMessage GetOrdersByDate(DateTime datetime)
        {
            using (DBORDERSEntities entitites = new DBORDERSEntities())
            {
                var entity = entitites.Orders.FirstOrDefault(e => e.OrderDate == datetime);

                if (entity != null)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, entity);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound,
                        "Order by date = " + datetime.ToString() + " not found");
                }
            }
        }


        /// <summary>
        /// Here u can create an order
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        public HttpResponseMessage Post([FromBody] Order order)
        {
            try
            {
                using (DBORDERSEntities entities = new DBORDERSEntities())
                {
                    entities.Orders.Add(order);
                    entities.SaveChanges();

                    var message = Request.CreateResponse(HttpStatusCode.Created, order);
                    message.Headers.Location = new Uri(Request.RequestUri + order.OrderID.ToString());
                    return message;
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        /// <summary>
        /// Update orders by id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="order"></param>
        /// <returns></returns>
        public HttpResponseMessage Put(int id, [FromBody]Order order)
        {
            try
            {
                using (DBORDERSEntities entitites = new DBORDERSEntities())
                {
                    var entity = entitites.Orders.FirstOrDefault(e => e.OrderID == id);

                    if (entity == null)
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Order with ID = " + id.ToString() + " not found to update");
                    }
                    else
                    {
                        entity.OrderID = order.OrderID;
                        entity.OrderDescription = order.OrderDescription;
                        entity.OrderDate = order.OrderDate;

                        entitites.SaveChanges();
                        return Request.CreateResponse(HttpStatusCode.OK, entity);
                    }
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }
    }
} 
