﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OrdersService.Models;
using OrdersService.Repositories;

namespace OrdersService.Processors
{
    public class OrdersProcessor
    {
        public static bool ProcessOrders(Order order)
        {
            return OrdersRepository.AddOrders(order);
        }
    }
}