﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OrdersService.Models
{
    public class Orders
    {
        public int Id { get; set; }
        public string OrderDescription { get; set; }
        public DateTime OrderDate { get; set; }
    }
}